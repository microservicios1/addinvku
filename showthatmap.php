<html lang="es">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" type="text/css" href="StRod.css">
    <title>Diagrama De Arquitectura</title>
    <?php
      include 'dbc.php';
      include 'session.php';
      $conn = mysqli_connect($host,$user,$pass,$db);
      if($_SERVER['REQUEST_METHOD']=="POST"&&isset($_POST['thatimage']))
      {
        $fl=$_POST['imag'];
        $isupp=0;
        $add="";
        $nimg = $_FILES['RR']['name'];
        $timg = $_FILES['RR']['type'];
        if ($nimg == !NULL)
        {
          $r= explode( '.', $fl ) ;
          if(($timg == "image/jpeg") || ($timg == "image/jpg") || ($timg == "image/png"))
          {
            if($timg == "image/jpeg")
            {
              $add="all-of-those-images/apr/".$r[0].".jpeg";
              $timg="jpeg";
            }
            if($timg == "image/jpg")
            {
              $add="all-of-those-images/apr/".$r[0].".jpg";
              $timg="jpg";
            }
            if($timg == "image/png")
            {
              $add="all-of-those-images/apr/".$r[0].".png";
              $timg="png";
            }
            $k=$r[0].".".$timg;
            $fl="";
          }
          else
          {
            $faceless = $_FILES['RR']['tmp_name'];
            if ($_FILES['RR']['error'] !== 0)
              echo 'Error al subir el archivo ';
            else
            {
              if (mime_content_type($_FILES['RR']['tmp_name']) == 'application/pdf')
              {
                $add="all-of-those-images/apr/".$r[0].".pdf";
                $timg="pdf";
                $k=$r[0].".".$timg;
                $fl="";
              }
              else
              {
                $fl="Formato no permitido";
                $isup=99;
              }
            }
          }
        }
        else
        {
          $fl="Ninguna imagen detectada en Diagrama de arquitectura";
          $isup=99;
        }
        if($isup==99)
          echo '<script type="text/javascript">alert("'.$fl.'");</script>';
        else
        {
          $fl="all-of-those-images/apr/".$_POST['imag'];
          unlink($fl);
          $sql="update proyectos set proyectos.DA='".$k."' where proyectos.no=".$_POST['no'];
          $re2=mysqli_query($conn,$sql);
          if(!$re2)
            echo "Conexion con BD fallida".mysqli_error();
          move_uploaded_file($_FILES["RR"]["tmp_name"],$add);
          echo  $_FILES["RR"]["tmp_name"].$add;
          echo "<form method=\"post\" action=\"showthatmap.php\" id=\"jumplikeaboss\" name=\"jumplikeaboss\"><input type=\"hidden\" name=\"no\" id=\"no\" value=\"".$_POST['no']."\">";
          if ($_POST['no']>0)
            echo "<script type=\"text/javascript\">document.getElementById('jumplikeaboss').submit();</script></form>";
        }
      }
    ?>
  </head>
  <body>
  <?php
    $sql = "select DA  from proyectos where no=".$_POST['no'];
    $re = mysqli_query($conn,$sql);
    if(!$re)
      echo "Conexion con BD fallida".mysqli_error();
    else
    {
      echo "<form class=\"container\" method=\"POST\" action=\"".htmlspecialchars($_SERVER['PHP_SELF'])."\" enctype=\"multipart/form-data\"><br><br><br><br><div class=\"menu\">Diagrama de arquitectura :<br><input type=\"file\" name=\"RR\" id=\"RR\"><button type=\"submit\"  name='thatimage' id='thatimage' >Remplazar mapa</button></div>";
      $row = mysqli_fetch_array($re);
      $v=$row['DA'];
      echo "<input type=\"hidden\" name=\"no\" id=\"no\" value=\"".$_POST['no']."\">";
      echo "<input type=\"hidden\" name=\"imag\" id=\"imag\" value=\"".$v."\">";
      $fechaSegundos = time();
      $strNoCache = "?nocache=$fechaSegundos";
      $r=explode('.',$v);
      if($r[1]=="pdf")
        echo "<iframe src=\"all-of-those-images/apr/".$v."\" width=\"100%\" height=\"80%\" </iframe></form>";
      else
        echo "<br><img style=\"width:80%;max-height:70%;\" src='all-of-those-images/apr/".$v.$strNoCache."' ></form>";
    }
    mysqli_close($conn);
  ?>
  </body>
</html>